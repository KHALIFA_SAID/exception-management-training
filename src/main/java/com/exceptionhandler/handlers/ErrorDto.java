package com.exceptionhandler.handlers;

import com.exceptionhandler.exception.ErrorCode;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorDto {
    private Integer status;
    private ErrorCode code;
    private String message;
    private Map<String, String> details;
}
