package com.exceptionhandler.controller;


import com.exceptionhandler.dto.ProductDto;
import com.exceptionhandler.exception.EntityNotFoundException;
import com.exceptionhandler.exception.ErrorCode;
import com.exceptionhandler.exception.InvalidEntityException;
import com.exceptionhandler.validator.ProductValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping(value = ProductController.ROOT_RESOURCE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

	public static final String ROOT_RESOURCE = "/api/products";

	public static final String PRODUCT_ID_PARAM = "productId";



	@PostMapping
	public ProductDto save(@RequestBody ProductDto articleDto) {
		validateArticle(articleDto);

		//Article article = articleDto.toEntity();
		//articleRepository.save(article);
		//return ArticleDto.fromEntity(articleRepository.save(articleDto.toEntity()));
		return articleDto;
	}

	private void validateArticle(ProductDto articleDto){
		Map<String,String> details = ProductValidator.validate(articleDto);
		if(!details.isEmpty()){
			log.error("Product is not valide {}",articleDto);
			throw new InvalidEntityException("Le produit n'est pas valid", ErrorCode.ARTICLE_NOT_VALID,details);
		}
	}
	@GetMapping(value = "/{" + ProductController.PRODUCT_ID_PARAM + "}")
	public ProductDto articleById(@PathVariable(value = "productId") Integer id) {
		log.error("Product is not valide {}",id);
		throw new EntityNotFoundException("Le produit non trouve", ErrorCode.ARTICLE_NOT_FOUND);
	}
}
