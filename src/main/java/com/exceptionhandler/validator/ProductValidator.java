package com.exceptionhandler.validator;

import com.exceptionhandler.dto.ProductDto;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class ProductValidator {

  public static Map<String,String> validate(ProductDto dto) {
    Map<String, String> details=new HashMap<>();
    if (dto == null) {
      details.put("codeProduct","Veuillez renseigner le code d produit");
      details.put("designation","Veuillez renseigner la designation de l'article");
      return details;
    }

    if (!StringUtils.hasLength(dto.getCodeProduct())) {
      details.put("codeProduct","Veuillez renseigner le code de l'article");
    }
    if (!StringUtils.hasLength(dto.getDesignation())) {
      details.put("designation","Veuillez renseigner la designation de l'article");
    }
    return details;
  }

}
