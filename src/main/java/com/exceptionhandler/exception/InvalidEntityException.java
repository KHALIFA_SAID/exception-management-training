package com.exceptionhandler.exception;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class InvalidEntityException extends RuntimeException {
    @Getter
    private ErrorCode errorCode;

    @Getter
    private Map<String, String> details=new HashMap<>();
    public InvalidEntityException(String message){
        super(message);
    }

    public InvalidEntityException(String message,Throwable cause){
        super(message,cause);
    }

    public InvalidEntityException(String message, Throwable cause, ErrorCode errorCode){
        super(message,cause);
        this.errorCode = errorCode;
    }

    public InvalidEntityException(String message, ErrorCode errorCode){
        super(message);
        this.errorCode = errorCode;
    }

    public InvalidEntityException(String message, ErrorCode errorCode, Map<String, String> details){
        super(message);
        this.errorCode = errorCode;
        this.details = details;
    }
}
