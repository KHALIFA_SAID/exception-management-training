package com.exceptionhandler.exception;



public enum ErrorCode {

    ARTICLE_NOT_VALID(1001),
    ARTICLE_NOT_FOUND(1000),
    ARTICLE_ALREADY_IN_USE(1002),



    CLIENT_NOT_FOUND(3000),
    CLIENT_NOT_VALID(3001),
    CLIENT_ALREADY_IN_USE(3002);

    private int code;

    ErrorCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
